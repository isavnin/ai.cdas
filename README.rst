AI.CDAS: python interface to `CDAS <http://cdaweb.gsfc.nasa.gov/>`_ data
=========================================================================

This library provides access to CDAS database from python in a simple and fluid way through `CDAS REST api <http://cdaweb.gsfc.nasa.gov/WebServices/REST/>`_. It fetches the data either in `CDF (Common Data Format) <http://cdf.gsfc.nasa.gov/>`_ or ASCII format and returns it in the form of dictionaries of numpy arrays.

The full documentation is available at `aicdas.rtfd.io <http://aicdas.rtfd.io>`_.

Getting started
===============

Dependencies
------------

-  Python 3
-  `numpy <http://www.numpy.org/>`_
-  `requests <http://docs.python-requests.org/en/latest/>`_
-  `wget <https://pypi.python.org/pypi/wget>`_

Extra dependencies (at least one of the following)
--------------------------------------------------

-  `astropy <http://www.astropy.org/>`_
-  `CDF <http://cdf.gsfc.nasa.gov/>`_ +
   `spacepy <http://spacepy.lanl.gov/doc/index.html>`_

Installation
------------

Starting from version 1.2.0 AI.CDAS officially supports only Python 3, so make sure that you have a working isntallation of it.

Assuming the above requirement is satisfied install the package with Python package manager::

    $ pip install ai.cs

Known issues
------------

NASA CDAS REST API endpoint currently does not support IPv6 addressing. However, newer linux distros (for example, Ubuntu 16.04) are set up to prefer IPv6 addressing over IPv4 by default. This may result in unneeded delays in communication with server and data polling. If you experience the issue it might be that is the case with your system. Here is how it can be cured on Ubuntu 16.04::

    $ sudoedit /etc/gai.conf
    # Uncomment the line
    # precedence ::ffff:0:0/96  100
 
Now you machine will try IPv4 prior to IPv6. For other distros refer to respective docs. 

Examples
--------

**Example 1**: Retrieving observatory groups and associated instruments which measure plasma and solar wind:

.. code-block:: python

    from ai import cdas
    import json # for pretty output

    obsGroupsAndInstruments = cdas.get_observatory_groups_and_instruments(
        'istp_public',
        instrumentType='Plasma and Solar Wind'
    )
    print(json.dumps(obsGroupsAndInstruments, indent=4))

**Example 2**: Getting STEREO-A datasets using regular expressions for dataset id and label:

.. code-block:: python

    from ai import cdas
    import json # for pretty output

    datasets = cdas.get_datasets(
        'istp_public',
        idPattern='STA.*',
        labelPattern='.*STEREO.*'
    )
    print(json.dumps(datasets, indent=4))

**Example 3**: Fetching a list of variables in one of STEREO datasets:

.. code-block:: python

    from ai import cdas
    import json # for pretty output

    variables = cdas.get_variables('istp_public', 'STA_L1_MAGB_RTN')
    print(json.dumps(variables, indent=4))

**Example 4**: This snippet of code gets magnetic field data from STEREO-A spacecraft for one hour of 01.01.2010 and plots it (requires matplotlib):

.. code-block:: python

    from ai import cdas
    from datetime import datetime
    from matplotlib import pyplot as plt

    data = cdas.get_data(
        'sp_phys',
        'STA_L1_MAG_RTN',
        datetime(2010, 1, 1),
        datetime(2010, 1, 1, 0, 59, 59),
        ['BFIELD']
    )
    plt.plot(data['EPOCH'], data['BTOTAL'])
    plt.show()

**Example 5**: This snippet of code gets magnetic field data from STEREO-A spacecraft for one hour of 01.01.2010 and plots it (requires matplotlib). The data are downloaded in CDF format in this case. CDF format is binary and results in a much smaller filesize and hence faster downloads. In order for this to work you have to have NASA CDF library on your machine and spacepy installed afterwards:

.. code-block:: python

    from ai import cdas
    from datetime import datetime
    from matplotlib import pyplot as plt

    data = cdas.get_data(
        'sp_phys',
        'STA_L1_MAG_RTN',
        datetime(2010, 1, 1),
        datetime(2010, 1, 1, 0, 59, 59),
        ['BFIELD'],
        cdf=True # download data in CDF format
    )
    # Note that variables identifiers are different than in the previous
    # example. It often the case with CDAS data. You should check the
    # variables names by printing out `data` dictionary.
    plt.plot(data['Epoch'], data['BFIELD'][:, 3])
    plt.show()

**Example 6**: This snippet of code gets magnetic field data from STEREO-A spacecraft for 01.01.2010 and saves it to cache directory. The next time the same data is requested it is taken from cache without downloading:

.. code-block:: python

    import os
    from ai import cdas
    from datetime import datetime

    # For the sake of example we are using your current working
    # directory as a cache directory
    cache_dir = os.getcwd()
    cdas.set_cache(True, cache_dir)
    # this data is downloaded from CDAS
    data = cdas.get_data(
        'sp_phys',
        'STA_L1_MAG_RTN',
        datetime(2010, 1, 1),
        datetime(2010, 1, 1, 0, 59, 59),
        ['BFIELD']
    )
    # this data is taken from cache
    data = cdas.get_data(
        'sp_phys',
        'STA_L1_MAG_RTN',
        datetime(2010, 1, 1),
        datetime(2010, 1, 1, 0, 59, 59),
        ['BFIELD']
    )
