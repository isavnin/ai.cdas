"""The module contains tests for ai.cs package."""
import os
from datetime import datetime
import socket
import shutil
from ai import cdas


def test_get_dataviews():
    dataviews = cdas.get_dataviews()
    assert isinstance(dataviews, dict)


def test_get_observatory_groups():
    observatoryGroups = cdas.get_observatory_groups("sp_phys")
    assert isinstance(observatoryGroups, dict)
    observatoryGroups = cdas.get_observatory_groups("sp_phys", instrumentType="Plasma and Solar Wind")
    assert isinstance(observatoryGroups, dict)


def test_get_instrument_types():
    instrumentTypes = cdas.get_instrument_types("sp_phys")
    assert isinstance(instrumentTypes, dict)
    instrumentTypes = cdas.get_instrument_types("sp_phys", observatory="Ahead")
    assert isinstance(instrumentTypes, dict)
    instrumentTypes = cdas.get_instrument_types("sp_phys", observatoryGroup="STEREO")
    assert isinstance(instrumentTypes, dict)
    instrumentTypes = cdas.get_instrument_types("sp_phys", observatory="Ahead", observatoryGroup="STEREO")
    assert isinstance(instrumentTypes, dict)


def test_get_instruments():
    instruments = cdas.get_instruments("sp_phys")
    assert isinstance(instruments, dict)
    instruments = cdas.get_instruments("sp_phys", observatory="Ahead")
    assert isinstance(instruments, dict)


def test_get_observatories():
    observatories = cdas.get_observatories("sp_phys")
    assert isinstance(observatories, dict)
    observatories = cdas.get_observatories("sp_phys", instrument="IMPACT_MAG")
    assert isinstance(observatories, dict)
    observatories = cdas.get_observatories("sp_phys", instrumentType="Plasma and Solar Wind")
    assert isinstance(observatories, dict)
    observatories = cdas.get_observatories("sp_phys", instrument="IMPACT_MAG", instrumentType="Plasma and Solar Wind")
    assert isinstance(observatories, dict)


def test_get_observatory_groups_and_instruments():
    observatoryGroupsAndInstruments = cdas.get_observatory_groups_and_instruments("sp_phys")
    assert isinstance(observatoryGroupsAndInstruments, dict)
    observatoryGroupsAndInstruments = cdas.get_observatory_groups_and_instruments(
        "istp_public", instrumentType="Plasma and Solar Wind"
    )
    assert isinstance(observatoryGroupsAndInstruments, dict)


def test_get_datasets():
    datasets = cdas.get_datasets("sp_phys")
    assert isinstance(datasets, dict)
    datasets = cdas.get_datasets(
        "sp_phys",
        observatoryGroup="STEREO",
        instrumentType="Plasma and Solar Wind",
        observatory="Ahead",
        instrument="IMPACT_MAG",
    )
    assert isinstance(datasets, dict)
    datasets = cdas.get_datasets(
        "sp_phys",
        observatoryGroup="STEREO",
        instrumentType="Plasma and Solar Wind",
        observatory="Ahead",
        instrument="IMPACT_MAG",
        startDate=datetime(2010, 1, 1),
        stopDate=datetime(2010, 1, 2),
    )
    assert isinstance(datasets, dict)
    datasets = cdas.get_datasets(
        "sp_phys",
        observatoryGroup="STEREO",
        instrumentType="Plasma and Solar Wind",
        observatory="Ahead",
        instrument="IMPACT_MAG",
        startDate=datetime(2010, 1, 1),
        stopDate=datetime(2010, 1, 2),
        idPattern=".+1HR$",
    )
    assert isinstance(datasets, dict)
    datasets = cdas.get_datasets(
        "sp_phys",
        observatoryGroup="STEREO",
        instrumentType="Plasma and Solar Wind",
        observatory="Ahead",
        instrument="IMPACT_MAG",
        startDate=datetime(2010, 1, 1),
        stopDate=datetime(2010, 1, 2),
        labelPattern=".+PLASTIC.+",
    )
    assert isinstance(datasets, dict)


def test_get_inventory():
    inventory = cdas.get_inventory("sp_phys", "STA_L2_PLA_1DMAX_1HR")
    assert isinstance(inventory, dict)


def test_get_variables():
    variables = cdas.get_variables("sp_phys", "STA_L2_PLA_1DMAX_1HR")
    assert isinstance(variables, dict)


def test_get_data():
    try:
        import spacepy

        data = cdas.get_data(
            "sp_phys",
            "STA_L2_PLA_1DMAX_1HR",
            datetime(2010, 1, 1),
            datetime(2010, 1, 2),
            ["proton_bulk_speed_1hr", "proton_temperature_1hr", "proton_thermal_speed_1hr"],
            cdf=True,
        )
        assert isinstance(data, dict)
    except ImportError:
        pass  # no testing needed if spacepy is not isntalled
    data = cdas.get_data(
        "sp_phys",
        "STA_L2_PLA_1DMAX_1HR",
        datetime(2010, 1, 1),
        datetime(2010, 1, 2),
        ["proton_bulk_speed_1hr", "proton_temperature_1hr", "proton_thermal_speed_1hr"],
        cdf=False,
    )
    assert isinstance(data, dict)
    cache_path = ".test_cache"
    if not os.path.exists(cache_path):
        os.mkdir(cache_path)
    cdas.set_cache(True, cache_path)
    data = cdas.get_data(
        "sp_phys",
        "STA_L2_PLA_1DMAX_1HR",
        datetime(2010, 1, 1),
        datetime(2010, 1, 2),
        ["proton_bulk_speed_1hr", "proton_temperature_1hr", "proton_thermal_speed_1hr"],
        cdf=False,
    )

    def guard(*args, **kwargs):
        raise Exception("I told you not to use the Internet!")

    socket_original = socket.socket
    socket.socket = guard
    data = cdas.get_data(
        "sp_phys",
        "STA_L2_PLA_1DMAX_1HR",
        datetime(2010, 1, 1),
        datetime(2010, 1, 2),
        ["proton_bulk_speed_1hr", "proton_temperature_1hr", "proton_thermal_speed_1hr"],
        cdf=False,
    )
    socket.socket = socket_original
    cdas.set_cache(False)
    shutil.rmtree(cache_path)
    assert isinstance(data, dict)
