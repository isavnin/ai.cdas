AI.CDAS: python interface to `CDAS <http://cdaweb.gsfc.nasa.gov/>`_ data
=========================================================================

This library provides access to CDAS database from python in a simple and fluid way through `CDAS REST api <http://cdaweb.gsfc.nasa.gov/WebServices/REST/>`_. It fetches the data either in `CDF (Common Data Format) <http://cdf.gsfc.nasa.gov/>`_ or ASCII format and returns it in the form of dictionaries of numpy arrays.

.. toctree::
   :maxdepth: 2
   
   getting_started
   api
